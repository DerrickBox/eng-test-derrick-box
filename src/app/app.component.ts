import { Component, OnInit, Renderer2, ElementRef,Directive, ViewChild } from '@angular/core';
import { environment } from '../environments/environment';
import { GetCoordinatesDataService } from './get-coordinates-data.service';
import { GetWeatherDataService } from './get-weather-data.service';
import { Weather } from './weather.model';
import { DisplayData } from './display-data.model';
import * as mapboxgl from 'mapbox-gl';
import { timer } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Random Weather Generator';
  showResults = false;
  loading = false;
  resultsShown = false;

  
  displayData = new DisplayData();
  displayArray = new Array<DisplayData>();

  //Map variables
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v9';

  //Coordinates for both map and weather
  lat = 0;
  lng = 0;

  //Weather object
  weather$ = new Weather();

  numberOfResults = -1;


  constructor(private getCoordinatesDataService: GetCoordinatesDataService,private getWeatherDataService: GetWeatherDataService, private renderer:Renderer2, private el: ElementRef){}

  ngOnInit(){
  }


  //Builds the weather results when the Generate button is pushed
  public buildResults(numberOfResults:number){
    this.loading = true;
    this.resultsShown = true;
    this.numberOfResults = numberOfResults;
    for(let i = 0; i < numberOfResults; i++){
      this.displayData = new DisplayData();
      this.callAPIs(this.displayData,i);
      
    }
  }


  //Sets the properties for map generaton
  private generateMap(displayData,i){
    (mapboxgl as any).accessToken = environment.mapbox.accessToken;
    let onlyTryOnce = true
    try{
      this.map = new mapboxgl.Map({
        container: 'map' + i,
        style: this.style,
        zoom: 1,
        center: [displayData.lng, displayData.lat]
      });

      // Add map controls
      this.map.addControl(new mapboxgl.NavigationControl());

      // create the marker
      new mapboxgl.Marker()
      .setLngLat([displayData.lng, displayData.lat])
      .addTo(this.map);
    }
    catch{
      if(onlyTryOnce){
        setTimeout(()=> this.generateMap(displayData,i), 1000)
        onlyTryOnce = false;
      }
      else{
        console.log('Map could not attach')
      }
    }

    return this.map;
  }

  //Converts the random nymbers into usable Lat/Long coords
  private numberFixer(number){
    number = number/1000000
    return number;
  }

  //Calls the Number services and triggers the calling of the Weather API
  private callAPIs(displayData,i){
    this.getCoordinatesDataService.getLatitude().subscribe(data => {
      displayData.lat = this.numberFixer(data),
      this.getCoordinatesDataService.getLongitude().subscribe(data => {
        displayData.lng = this.numberFixer(data),
        this.callWeather(displayData,i);
        return this.weather$;
      });
    });
  }

  //Calls the weather service and sets weather data
  private callWeather(displayData,i){
    this.getWeatherDataService.getWeather(displayData.lat, displayData.lng).subscribe(data => {
      this.weather$ = data;
      displayData.description = this.weather$.weather[0]["description"];
      displayData.temp = this.weather$.main["temp"];
      displayData.feels_like = this.weather$.main["feels_like"];
      displayData.temp_max = this.weather$.main["temp_max"];
      displayData.temp_min = this.weather$.main["temp_min"];
      displayData.pressure = this.weather$.main["pressure"];
      displayData.humidity = this.weather$.main["humidity"];
      displayData.wind = this.weather$.wind["speed"];
      displayData.sunrise = this.timeConverter(this.weather$.sys["sunrise"]);
      displayData.sunset = this.timeConverter(this.weather$.sys["sunset"]);  
      displayData.mapValue = i;   
      this.displayArray.push(displayData)
      displayData.map = this.generateMap(displayData,i); 

      if(i == this.numberOfResults-1)
        this.loading = false;
    });    
  }
  
  //Converts from Epoch time to AM/PM
  private timeConverter(Epoch){

    let date = new Date(0);
    date.setUTCSeconds(Epoch);

    return date.toLocaleTimeString();
  }

}
