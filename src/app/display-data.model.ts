export class DisplayData {
    map: mapboxgl.Map;
    lat: number;
    lng: number;
    description: string;
    temp: string;
    feels_like: string;
    temp_max: string;
    temp_min: string;
    pressure: string;
    humidity: string;
    wind: string;
    sunrise: string;
    sunset: string;
    mapValue:number;
}