//Model for the weather data returned
export class Weather {
    weather: Array<JSON>;
    main:object[];
    wind:object[];
    clouds:object[];
    sys:object[];
    
}