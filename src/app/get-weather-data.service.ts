import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Weather } from './weather.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetWeatherDataService {

  weatherUrl = '';

  constructor(private _http: HttpClient) { }

  //Calls the Weather API using the lattitude and longitude from the random numbers and returns an observable of Weather
  getWeather(lat, long){
    this.weatherUrl = 'https://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + long + '&appid=' + environment.openWeather.key + '&units=imperial';
    return this._http.get<Weather>(this.weatherUrl);
  }
}
