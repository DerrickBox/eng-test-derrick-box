import { TestBed } from '@angular/core/testing';

import { GetCoordinatesDataService } from './get-coordinates-data.service';

describe('GetCoordinatesDataService', () => {
  let service: GetCoordinatesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetCoordinatesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
