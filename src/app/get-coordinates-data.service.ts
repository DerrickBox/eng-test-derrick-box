import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetCoordinatesDataService {
  //URL to generate a random number from -90000000 to 90000000 to be later converted into -90.000000 to 90.000000
  LatitudeUrl = 'https://www.random.org/integers/?num=1&min=-90000000&max=90000000&col=5&base=10&format=plain&rnd=new';
  //URL to generate a random number from -180000000 to 180000000 to be later converted into -180.000000 to 180.000000
  LongitudeUrl = 'https://www.random.org/integers/?num=1&min=-180000000&max=180000000&col=5&base=10&format=plain&rnd=new';

  constructor(private _http: HttpClient) { }

  //Calls the random number API 
  getLatitude(){
    return this._http.get(this.LatitudeUrl);
  }
  getLongitude(){
    return this._http.get(this.LongitudeUrl);
  }
}
