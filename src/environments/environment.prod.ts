export const environment = {
  production: true,

  openWeather:{
    key: 'c127e12a6f09daaffada7581f79471f7'
  },

  mapbox:{
    accessToken:'pk.eyJ1IjoiZGVycmlja2JveCIsImEiOiJjazcyeXhldGEwN2hzM2RyOXlpNHhhOGthIn0.SqD1vsdutGJ5ix50RnEVNA'
  }
};
