Program by Derrick Box


## Setup
Download and unzip
Run `npm i` to install all necessary libraries in unzipped folder**


## Server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
or
`ng serve --open` to start the server and automatically open a web page

**Will need Angular installed using `npm install -g @angular/cli`